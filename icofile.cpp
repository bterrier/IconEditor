#include <QDataStream>
#include <QDebug>
#include <QFile>
#include <QLabel>
#include <QPixmap>
#include <QRgb>

#include "icofile.h"

#include <cstring>




IcoFile::IcoFile(const QString &file)
{
    if( !file.isEmpty() )
    {
        QFile f(file);
        if(f.open(QFile::ReadOnly))
        {
            QByteArray icondir = f.read(6);
            if( icondir.size() < 6 )
                return;

            quint16 s = ( icondir[0] ) | ( icondir[1] << 8  );
            if(s != 0)
                return;

            quint16 type = ( icondir[2] ) | ( icondir[3] << 8  );
            if(type != 1)
                return;

            quint16 count = ( icondir[4] ) | ( icondir[5] << 8  );
            qDebug() << count;

            _count = count;

            for( int i = 0 ; i < _count ; ++i )
            {
                qDebug() << "Entry:" << i;
                f.seek(6+16*i);
                QByteArray icondirentry = f.read(16);
                if(icondirentry.size() < 16 )
                {
                    qCritical() << "Corrupted file!";
                }
                int width = static_cast<quint8>(icondirentry[0]);
                if( width == 0 )
                    width = 256;

                int height = static_cast<quint8>(icondirentry[1]);
                if( height == 0 )
                    height = 256;

                //quint8 palette = icondirentry[2];
                quint16 color_planes = ( icondirentry[4] ) | ( icondirentry[5] << 8  );
                quint16 bits_per_pixel = ( icondirentry[6] ) | ( icondirentry[7] << 8  );
                if(color_planes > 1)
                    bits_per_pixel *= color_planes;
                quint32 data_size = readLE<quint32>(icondirentry, 8);
                qDebug() << "Data size:" << data_size;
                qDebug() << icondirentry.toHex();
                quint32 data_offset = readLE<quint32>(icondirentry, 12);
                qDebug() << "Data offset:" << data_offset;
                qDebug() << hex << data_offset;
                if( ! f.seek(data_offset) )
                {
                    return;
                }

                qDebug() << "pos:" << f.pos();
                qDebug() << "fs:" << f.size();
                if(f.atEnd())
                {
                    qDebug() << "At end";
                    continue;
                }
                QByteArray image_data = f.read(data_size);
                if(image_data.size()  != static_cast<qint64>(data_size))
                    return;

                _entries.append(IcoEntry(image_data, QSize(width, height), bits_per_pixel));
            }
            f.close();
        }
    }
}
QList<IcoEntry> IcoFile::entries() const
{
    return _entries;
}

void IcoFile::setEntries(const QList<IcoEntry> &entries)
{
    _entries = entries;
    _count =entries.count();
}
int IcoFile::count() const
{
    return _count;
}

void IcoFile::setCount(int count)
{
    _count = count;
}

void IcoFile::save(const QString &filePath) const
{
    if( !filePath.isEmpty() )
    {
        QFile f(filePath);
        if(f.open(QFile::WriteOnly))
        {
            QByteArray icondir(6, 0);
            icondir[2] = 1;
            quint16 count = _count;
            icondir[4] = count & 0xFF;
            icondir[5] = count >> 8 & 0xFF;
            f.write(icondir);
            quint32 bufferPos = 6 + _count * 16;
            for( int i = 0 ; i < _count ; ++i )
            {
                QByteArray icondirentry(16, 0);
                QSize size = _entries[i].size();
                icondirentry[0] = size.width() > 255 ? 0 : size.width();
                icondirentry[1] = size.height() > 255 ? 0 : size.height();

                icondirentry[3] = 0;
                quint32 dataSize = _entries[i].data().size();
                icondirentry[6] = _entries[i].bitsPerPixel() & 0xFF;
                icondirentry[7] = _entries[i].bitsPerPixel()>>8 & 0xFF;
                icondirentry[8] = dataSize & 0xFF;
                icondirentry[9] = dataSize>>8 & 0xFF;
                icondirentry[10] = dataSize>>16 & 0xFF;
                icondirentry[11] = dataSize>>24 & 0xFF;
                icondirentry[12] = bufferPos & 0xFF;
                icondirentry[13] = bufferPos>>8 & 0xFF;
                icondirentry[14] = bufferPos>>16 & 0xFF;
                icondirentry[15] = bufferPos>>24 & 0xFF;
                bufferPos += dataSize;
                f.write(icondirentry);
            }
            for( int i = 0 ; i < _count ; ++i )
            {
                f.write(_entries[i].data());
            }
            f.close();
        }
    }
}



