#ifndef ICOENTRY_H
#define ICOENTRY_H

#include <QByteArray>
#include <QPixmap>

class IcoEntry
{
public:
    enum Format {
        Unknown,
        PNG,
        BMP
    };

    IcoEntry(const QByteArray &data, const QSize &size, int bitsPerPixel);

    Format format() const;
    void setFormat(const Format &format);

    QPixmap pixmap() const;
    void setPixmap(const QPixmap &pixmap);

    QSize size() const
    {
        return _pixmap.size();
    }


    QByteArray data() const;

    int bitsPerPixel() const;

private:
    Format _format;
    QPixmap _pixmap;
    QByteArray _data;
    int m_bitsPerPixel;
};

template<typename T>
T readLE(const QByteArray &data, int pos)
{
    T t = 0;
    for(int i = 0 ; i < static_cast<int>(sizeof(T)) ; ++i )
    {
        t = t | (( data[pos+i] & 0xFF ) << (8*i) );
    }
    return t;
}

#endif // ICOENTRY_H
