#include "icoentry.h"

#include <cstring>

#include <QDebug>
#include <QImage>

uchar PNG_SIGNATURE[] = {0x89,0x50,0x4E,0X47,0x0D,0x0A,0x1A,0x0A};

IcoEntry::IcoEntry(const QByteArray &data, const QSize &size, int bitsPerPixel) :
    _data(data),
    m_bitsPerPixel(bitsPerPixel)
{
    QImage image;
    if( 0 == std::memcmp(_data.constData(), PNG_SIGNATURE, 8) )
    {
        qDebug() << "PNG";
        image = QImage::fromData(_data, "PNG");
        _format = PNG;
    }
    else
    {
        _format = BMP;
        qDebug() << "BMP";
        QByteArray header;
        header.resize(14);
        header[0] = 0x42;
        header[1] = 0x4D;
        quint32 file_size = 14 + _data.size();
        qDebug() << "BMP file size:" << file_size;
        header[2] = file_size & 0xFF;
        header[3] = (file_size>>8) & 0xFF;
        header[4] = (file_size>>16) & 0xFF;
        header[5] = (file_size>>24) & 0xFF;
        qDebug() << *(quint32 *) (header.constData()+2);
        header[6] = 0;
        header[7] = 0;
        header[8] = 0;
        header[9] = 0;
        quint32 bmp_header_size = readLE<quint32>(_data,0);
        quint32 offset = bmp_header_size + 0x0e;
        qDebug() << hex << offset;
        header[10] = offset & 0xFF;
        header[11] = (offset>>8) & 0xFF;
        header[12] = (offset>>16) & 0xFF;
        header[13] = (offset>>24) & 0xFF;
        header.append(_data);
        //qDebug() << header.toHex();
        qDebug() << "will convert";
        image = QImage::fromData(header, "BMP");
        qDebug() << "converted";

        QImage img(size, QImage::Format_ARGB32);

        if(bitsPerPixel == 32)
        {
            int i = bmp_header_size;
            for(int row = size.height() - 1 ; row >= 0 ; --row )
            {
                for( int col = 0 ; col < size.width() ; ++col )
                {
                    img.setPixel(col, row, qRgba(_data[i+2], _data[i+1], _data[i], _data[i+3]));

                    i+=4;
                }
            }
        }

        image = img;

    }

    _pixmap = QPixmap::fromImage(image);
}

IcoEntry::Format IcoEntry::format() const
{
    return _format;
}

void IcoEntry::setFormat(const Format &format)
{
    _format = format;
}
QPixmap IcoEntry::pixmap() const
{
    return _pixmap;
}

void IcoEntry::setPixmap(const QPixmap &pixmap)
{
    _pixmap = pixmap;
}

QByteArray IcoEntry::data() const
{
    return _data;
}

int IcoEntry::bitsPerPixel() const
{
    return m_bitsPerPixel;
}



