#ifndef ICOFILE_H
#define ICOFILE_H

#include <QList>
#include <QString>

#include "icoentry.h"



class IcoFile
{
public:
    IcoFile(const QString &file = QString());

    QList<IcoEntry> entries() const;
    void setEntries(const QList<IcoEntry> &entries);

    int count() const;
    void setCount(int count);

    void save(const QString &filePath)const;

private:
    int _count = 0;
    QList<IcoEntry> _entries;
};

#endif // ICOFILE_H
