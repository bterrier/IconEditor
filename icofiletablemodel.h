#ifndef ICOFILETABLEMODEL_H
#define ICOFILETABLEMODEL_H

#include <QAbstractTableModel>
#include "icofile.h"
class IcoFileTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit IcoFileTableModel(IcoFile file, QObject *parent = 0);

signals:

public slots:

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    void setIcoFile(const IcoFile &file);
    const IcoFile &icoFile() const
    {
        return _file;
    }

    void addImage(const QString &filePath);

private:
    IcoFile _file;

    // QAbstractItemModel interface
public:
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
};

#endif // ICOFILETABLEMODEL_H
