#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QString>

#include <QMainWindow>

class IcoFileTableModel;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionNew_triggered();

    void on_actionOpen_triggered();

    void on_actionSave_triggered();

    void on_actionSaveAs_triggered();

    void on_actionAdd_triggered();

    void on_actionQuit_triggered();

private:
    Ui::MainWindow *ui;
    QString m_file;
    QString m_currentDir;
    IcoFileTableModel *m_model = nullptr;

};

#endif // MAINWINDOW_H
