#include "icofiletablemodel.h"

#include <QFile>
#include <QStringBuilder>

IcoFileTableModel::IcoFileTableModel(IcoFile file, QObject *parent) :
    QAbstractTableModel(parent),
    _file(file)
{
}



int IcoFileTableModel::rowCount(const QModelIndex &parent) const
{
    if(parent.isValid())
        return 0;

    return _file.count();
}

int IcoFileTableModel::columnCount(const QModelIndex &parent) const
{
    if(parent.isValid())
        return 0;

    return 3;
}

QVariant IcoFileTableModel::data(const QModelIndex &index, int role) const
{
    switch(index.column())
    {
    case 0:

        switch(role)
        {
        case Qt::DecorationRole:
            return _file.entries().at(index.row()).pixmap();
        }
        break;
    case 1:
        switch(role)
        {
        case Qt::DisplayRole:
        {
            QSize size =_file.entries().at(index.row()).size();
            QString str = QString::number(size.width())% 'x' % QString::number(size.height());
            return str;
        }
        }
        break;
    case 2:
        switch(role)
        {
        case Qt::DisplayRole:
        {
            switch(_file.entries().at(index.row()).format())
            {
            case IcoEntry::BMP:
                return "Bitmap";
            case IcoEntry::PNG:
                return "PNG";
            case IcoEntry::Unknown:
                return tr("Unknown");
            }

        }
        }
        break;
    }
    return QVariant();
}

void IcoFileTableModel::setIcoFile(const IcoFile &file)
{
    beginResetModel();
    _file = file;
    endResetModel();
}

void IcoFileTableModel::addImage(const QString &filePath)
{
    QFile f(filePath);
    if(f.open(QFile::ReadOnly))
    {
        beginInsertRows(QModelIndex(), _file.count(), _file.count());

        auto list =_file.entries();
        IcoEntry entry(f.readAll(), QSize(), 0) ;
        list.append(entry);
        _file.setEntries(list);
        endInsertRows();
        f.close();
    }
}


QVariant IcoFileTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if( orientation == Qt::Horizontal && role == Qt::DisplayRole )
    {
        switch (section)
        {
        case 0:
            return tr("Preview");
        case 1:
            return tr("Size");
        case 2:
            return tr("Format");
        }
    }
    return QAbstractTableModel::headerData(section, orientation, role);
}
