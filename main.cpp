#include <QApplication>

#include "mainwindow.h"
#include "icofile.h"

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

    app.setApplicationName("IconEditor");
    app.setApplicationDisplayName("Icon Editor");

    MainWindow window;
    window.show();

    return app.exec();
}
