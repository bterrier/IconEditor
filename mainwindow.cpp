#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QStandardPaths>

#include <QFileDialog>

#include "icofiletablemodel.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_currentDir(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation))
{
    ui->setupUi(this);

    m_model = new IcoFileTableModel(IcoFile(), this);
    ui->tableView->setModel(m_model);

    connect(ui->actionAboutQt, &QAction::triggered, qApp, &QApplication::aboutQt);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionNew_triggered()
{
    m_file.clear();
    setWindowFilePath(m_file);
    setWindowModified(false);
}

void MainWindow::on_actionOpen_triggered()
{
    QString filePath = QFileDialog::getOpenFileName(this, tr("Open ..."), m_currentDir );

    if( filePath.isEmpty() )
    {
        return;
    }

    m_file = filePath;
    setWindowFilePath(m_file);
    setWindowModified(false);
    m_model->setIcoFile(IcoFile(m_file));
    m_currentDir = QFileInfo(filePath).path();
}

void MainWindow::on_actionSave_triggered()
{
    if(m_file.isEmpty())
    {
        on_actionSaveAs_triggered();
    }
    else
    {
        m_model->icoFile().save(m_file);
    }
}

void MainWindow::on_actionSaveAs_triggered()
{
    QString filePath = QFileDialog::getSaveFileName(this, tr("Save As..."), m_currentDir);
    if(filePath.isEmpty())
    {
        return;
    }

    m_model->icoFile().save(filePath);

    m_currentDir = QFileInfo(filePath).path();
    m_file = filePath;
    setWindowFilePath(filePath);
}

void MainWindow::on_actionAdd_triggered()
{
    QString filePath = QFileDialog::getOpenFileName(this, tr("Open"), m_currentDir);
    if(filePath.isEmpty())
        return;


    m_model->addImage(filePath);
}

void MainWindow::on_actionQuit_triggered()
{
    close();
}
